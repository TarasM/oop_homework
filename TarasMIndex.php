<?php

Interface giveInformationInterface
{
    public function giveInformation();
}
abstract class Person implements giveInformationInterface
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
}
class Worker extends Person
{
    private $department;

    public function __construct($name, $department)
    {
        parent::__construct($name);
        $this->department = $department;
    }
    public function getDepartment()
    {
        return $this->department;
    }
    public function setDepartment($department)
    {
        $this->department = $department;
    }
    public function giveInformation()
    {
        echo "I am ".get_class($this)." of ".$this->getDepartment()." department, my name is: ".$this->getName().".\n";
    }
    public function doWork()
    {
        echo get_class($this)." ".$this->getName()." working.\n";
    }
}
class Chief extends Worker
{
    public function __construct($name, $department)
    {
        parent::__construct($name, $department);
    }
    public function makeOrder(Worker $Worker)
    {
        if($Worker)
        {
            echo $this->getName().", ".get_class($this)." of ".$this->getDepartment()." department, says to ".$Worker->getName()." : do your work!\n";
            if($Worker->getDepartment() == $this->getDepartment())
            {
                $Worker->doWork();
            }
            else
            {
                echo $Worker->getName().", ".get_class($Worker)." of ".$Worker->getDepartment()." department, says: you are not my Chief!\n";
            }
        }
    }
}

$Chief = new Chief("Mark", "Financial");
$Worker_1 = new Worker("Alex", "Financial");
$Worker_2 = new Worker("Vlad", "Contact with clients");
$Chief->giveInformation();
$Worker_1->giveInformation();
$Worker_2->giveInformation();
$Chief->makeOrder($Worker_1);
$Chief->makeOrder($Worker_2);